angular.module('autoSuggestApp', ['ionic'])
  .controller('AutoSuggestCtrl', function($scope, foodFactory) {
    $scope.suggest = true;
    
    $scope.preferences = {
      meat: true,
      exotic: true,
      health: 0,
      starve: 0
    };

    $scope.$watch('preferences', function() {
      $scope.foodList = foodFactory.getSuggestedFoodList($scope.preferences);
    }, true);
  })

  .filter('between', function() {
    return function(input, a, b) {
      return input >= a && input < b;
    };
  })

  .factory('foodFactory', function($parse) {
    var foodList = [
      {
        title: 'Fastfood burger',
        desc: 'Very lardy beef burger!',
        image: 'stylesheets/img/1.jpg',
        rule: 'meat === true && health < -40 && starve > 40'
      },
      {
        title: 'Sushi time!',
        desc: 'Delicious sushi okinawa style!',
        image: 'stylesheets/img/2.jpg',
        rule: 'exotic === true && meat === true && (health | between:20:80) && (starve | between:0:60)'
      },
      {
        title: 'Fiery meet in a pan',
        desc: 'This is increbibly hot man!',
        image: 'stylesheets/img/3.jpg',
        rule: 'meat === true && (health | between:0:60) && (starve | between:0:50)'
      },
      {
        title: 'Cheese bread meltdown',
        desc: 'Melted cheese with cheese on top of cheese...',
        image: 'stylesheets/img/4.jpg',
        rule: '(health | between:-50:30) && starve > 20'
      },
      {
        title: 'Vegies in a basket',
        desc: 'They are raw, they are cold and they are healthy!',
        image: 'stylesheets/img/5.jpg',
        rule: 'health > 40 && starve < 30'
      },
      {
        title: 'Bread and butter',
        desc: 'Good old bread and butter',
        image: 'stylesheets/img/6.jpg',
        rule: '(health | between:-20:20) && (starve | between:0:30)'
      }
    ];
    
    return {
      getFoodList: function() {
        return foodList;
      },
      getSuggestedFoodList: function(context) {
        return foodList.filter(function(food) {
          return $parse(food.rule)(context);
        });
      }
    };
  });
