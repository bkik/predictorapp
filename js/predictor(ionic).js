angular.module('diseasePredictorApp', ['ionic'])
  .controller('diseasePredictorCtrl', function($scope, diseaseFactory) {
    $scope.suggest = true;
    
    $scope.symptoms = {
      cough: false,
      runny_nose: false,
      sore_throat: false,
      sore_muscles: false,
      tiredness: false,
      headache: false,
      chills: false,
      fever: false,
      stomach_pain: false,
      diarrhoea: false,
      vomitting: false,
      dehydration: false,
      nausea: false,
      appetite_loss: false,
      sweating: false,
      chest_pain: false,
      //few_bowel_movt: false,
      headache: 0,
      pain: 0,
      tiredness: 0
    };

    $scope.$watch('symptoms', function() {
    $scope.diseaseList = diseaseFactory.getSuggestedDiseaseList($scope.symptoms);
    }, true);
  })

  .filter('between', function() {
    return function(input, a, b) {
      return input >= a && input < b;
    };
  })

  .factory('diseaseFactory', function($parse) {
    var diseaseList = [
      {
        title: 'Common Cold',
        desc: 'acute viral nasopharyngitis', 
        type: 'Viral Infection' ,
        cause: 'rhinoviruses spread through the air during close contact with infected people',
                      meds:  'Coldcap' ,
        image: 'stylesheets/img/1.jpg',
        rule: 'cough == true && runny_nose == true && sore_throat == true || headache == true || tiredness == true && (headache | between:0:3) && (tiredness | between:0:3) && (pain | between:0:3)'
      },
      {
        title: 'Malaria',
        desc: 'plasmodium malariae',
        type: 'Parasitic Infection' ,
        cause: 'Parasitic protozoans belonging to the group Plasmodium type transmitted by an infected female Anopheles mosquito',
                      meds:  'antimalarials' , 
                      image: 'stylesheets/img/2.jpg',
        rule: 'headache == true && sore_muscles == true && chills == ture && fever == true || tiredness == true && (headache | between:7:10) && (tiredness | between:6:10) && (pain | between:7:10)'
      }, 
      {
        title: 'Food Poisoning',
        desc: 'This is increbibly baaaaad',
        type: '' ,
        cause: '' ,
                      meds:  'Coldcap' ,                    
        image: 'stylesheets/img/3.jpg',
        rule: 'stomach_pain == true && diarrhoea == true && vomitting == true && nausea == true || dehydration == true || appetite_loss == true'
      },
      {
        title: 'Typhoid',
        desc: 'Salmonella typhi',
        type: '' ,
        cause: '' ,
                      meds:  'Coldcap' ,
        image: 'stylesheets/img/4.jpg',
        rule: 'diarrhoea == true && fever == true && sore_muscles == true && stomach_pain == true || appetite_loss == true && (headache | between:5:7) && (pain | between:5:7)'
      },
      {
        title: 'Pneumonia',
        desc: 'You are not healthy',
        type: '' ,
        cause: '' ,
                      meds:  'Coldcap' ,
        image: 'stylesheets/img/5.jpg',
        rule: 'chest_pain == true && chills == true && sweating == true && fever == true || tiredness == true || nausea == true || vomitting == true || diarrhoea == true && (tiredness | between:3:5)'
      }
    ];
    
    return {
      getDiseaseList: function() {
        return diseaseList;
      },
      getSuggestedDiseaseList: function(context) {
        return diseaseList.filter(function(disease) {
          return $parse(disease.rule)(context);
        });
      }
    };
  });
